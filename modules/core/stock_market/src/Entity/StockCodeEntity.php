<?php

namespace Drupal\stock_market\Entity;

class StockCodeEntity extends \Entity
{

  /**
   * Lấy giá tham chiếu của cổ phiếu
   */
  public function getReferencePrice() {
    return $this->reference_price ? $this->reference_price : 0;
  }

  /**
   * Tính giá trần của cổ phiếu
   */
  public function getCeilPrice() {
    return $this->reference_price + ($this->reference_price * stock_market_get_market_settings('price_range') / 100);
  }

  /**
   * Tính giá sàn của cổ phiếu
   */
  public function getFloorPrice() {
    return $this->reference_price - ($this->reference_price * stock_market_get_market_settings('price_range') / 100);
  }

}
