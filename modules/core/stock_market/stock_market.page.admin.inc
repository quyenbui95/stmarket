<?php

/**
 * Form khởi tạo thị trường
 * Cung cấp 2 bước thực hiện
 * - Đầu tiên là lựa chọn những mã nào sẽ cho phép đầu tư
 * - Nhập giá tham chiếu cho từng mã
 */
function stock_market_initialize_market($form, &$form_state) {
  // Cấu hình đã có trên hệ thống trong những lần cấu hình trước
  $market_option = variable_get('stock_market_market_option', array());

  // Lấy ra toàn bộ những mã chứng khoán đang có trong hệ thống
  if (!isset($form_state['stock_codes'])) {
    $form_state['stock_codes'] = array();
    $query = db_select('stock_code');
    $query->fields('stock_code', array('stock_code'));
    $result = $query->execute()->fetchAll();
    foreach ($result as $code) {
      $form_state['stock_codes'][$code->stock_code] = stock_code_load($code->stock_code);
    }
  }

  $form['description_top'] = array(
    '#markup' => t('Cẩn thận: Khi khởi tạo thị trường nếu hệ thống đang được mở cửa thì toàn bộ dữ liệu về người dùng và dữ liệu thị trường hiện tại sẽ không được bảo lưu'),
  );

  // vertical_tabs field group
  $form['market_group'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['stock_code'] = array(
    '#type' => 'fieldset',
    '#title' => t('Niêm yết mã chứng khoán.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => $form_state['stock_codes'] ? t('Lựa chọn những mã chứng khoán sẽ niêm yết trên thị trường.') : t('Hiện tại chưa có cổ phiếu nào tồn tại trên hệ thống'),
    '#group' => 'market_group',
  );

  // Tạo field để chọn mã chứng khoán
  $form['stock_code']['stock_code'] = array(
    '#tree' => TRUE,
    '#type' => 'container',
    '#prefix' => '<div id="group_stock_code_wrapper">',
    '#suffix' => '</div>'
  );

  if ($form_state['stock_codes']) {
    foreach ($form_state['stock_codes'] as $stock_code) {
      $form['stock_code']['stock_code'][$stock_code->stock_code] = array('#type' => 'container');
      // Field mã chứng khoán
      $form['stock_code']['stock_code'][$stock_code->stock_code]['value'] = array(
        '#type' => 'checkbox',
        '#title' => '<strong>' . $stock_code->stock_code . '</strong>: ' . $stock_code->company_name,
        '#ajax' => array(
          'callback' => 'stock_market_initialize_market_stock_code_ajax',
          'wrapper' => 'group_stock_code_wrapper',
        ),
      );

      // Field giá tham chiếu
      // Field này sẽ chỉ hiện lên khi mã chứng khoán được chọn
      $form['stock_code']['stock_code'][$stock_code->stock_code]['reference_price'] = array(
        '#type' => 'textfield',
        '#title' => t("Giá tham chiếu (x 1000)"),
        '#required' => TRUE,
        '#access' => isset($form_state['values']['stock_code'][$stock_code->stock_code]['value']) ? $form_state['values']['stock_code'][$stock_code->stock_code]['value'] === 1 : FALSE
      );
    }
  }

  // Cấu hình thị trường
  $form['market_option'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cấu hình thị trường.'),
    '#description' => t('Thị trường chứng khoán mỗi phiên sẽ bao gồm 3 giai đoạn. Giao đoạn ATO, Khớp lệnh liên tục và ATC. Mỗi giai đoạn sẽ có thời gian cụ thể, Khoảng trống thời gian từng giai đoạn sẽ là thời gian nghỉ giao dịch giữa giờ'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'market_group',
    '#tree' => TRUE
  );

  // Tạo field để chọn mã chứng khoán
  $form['market_option']['in_day'] = array(
    '#type' => 'select',
    '#title' => t('Số phiên giao dịch trong ngày'),
    '#options' => array(
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4
    ),
    '#ajax' => array(
      'callback' => 'stock_market_initialize_market_trading_sessions_ajax',
      'wrapper' => 'market-option-trading-type-wrapper',
    ),
    '#default_value' => isset($market_option['in_day']) ? $market_option['in_day'] : NULL
  );

  $form['market_option']['trading_sessions'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="market-option-trading-type-wrapper">',
    '#suffix' => '</div>'
  );

  // Cấu hình thời gian và kiểu khớp lệnh
  if (isset($form_state['values']['market_option']['in_day'])) {
    $tradding_sessions_in_day = $form_state['values']['market_option']['in_day'];
  }
  else {
    $tradding_sessions_in_day = !empty($market_option['in_day']) ? $market_option['in_day'] : 1;
  }

  for ($i = 1; $i <= $tradding_sessions_in_day; $i++) {
    // Thời gian AtO
    $form['market_option']['trading_sessions'][$i]['trading_type']['ato'] = array(
      '#type' => 'textfield',
      '#title' => t('Thời gian ATO'),
      '#description' => t('Định dạng: giờ:phút AM/PM, Ví dụ @example', array('@example' => date('h:i A', time()))),
      '#required' => TRUE,
      '#default_value' => isset($market_option['trading_sessions'][$i]['trading_type']['ato']) ? $market_option['trading_sessions'][$i]['trading_type']['ato'] : NULL,
      '#prefix' => t('<h3>Phiên số @session</h3>', array('@session' => $i)),
    );
    // Khớp liên tục
    $form['market_option']['trading_sessions'][$i]['trading_type']['continuous_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Thời gian khớp lệnh liên tục'),
      '#description' => t('Định dạng: giờ:phút AM/PM, Ví dụ @example', array('@example' => date('h:i A', time()))),
      '#default_value' => isset($market_option['trading_sessions'][$i]['trading_type']['continuous_time']) ? $market_option['trading_sessions'][$i]['trading_type']['continuous_time'] : NULL,
      '#required' => TRUE
    );
    // ATC
    $form['market_option']['trading_sessions'][$i]['trading_type']['atc'] = array(
      '#type' => 'textfield',
      '#title' => t('Thời gian ATC'),
      '#description' => t('Định dạng: giờ:phút AM/PM, Ví dụ @example', array('@example' => date('h:i A', time()))),
      '#default_value' => isset($market_option['trading_sessions'][$i]['trading_type']['atc']) ? $market_option['trading_sessions'][$i]['trading_type']['atc'] : NULL,
      '#required' => TRUE
    );
  }

  // Ngày mở cửa giao dịch
  $form['market_option']['open_days'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Ngày mở cửa giao dịch trong tuần',
    '#description' => t('Những ngày trong tuần sẽ mở cửa cho phép nhà đầu tư giao dịch.'),
    '#options' => array(
      'Mon' => t('Thứ Hai'),
      'Tue' => t('Thứ Ba'),
      'Wed' => t('Thứ Tư'),
      'Thu' => t('Thứ Năm'),
      'Fri' => t('Thứ Sáu'),
      'Sat' => t('Thứ Bảy'),
      'Sun' => t('Chủ Nhật'),
    ),
    '#default_value' => isset($market_option['open_days']) ? $market_option['open_days'] : array(),
    '#required' => TRUE
  );

  // Kiểu khớp lệnh
  $form['market_option']['settlement_period'] = array(
    '#type' => 'select',
    '#title' => 'Kiểu thời gian thanh toán',
    '#description' => t('Chu kỳ để thanh số tiền hoặc cổ phiếu về tài khoản người chơi khi khớp lệnh thành công.'),
    '#options' => array(
      '1' => t('T+1'),
      '2' => t('T+2'),
      '3' => t('T+3'),
    ),
    '#default_value' => isset($market_option['settlement_period']) ? $market_option['settlement_period'] : array(),
    '#required' => TRUE
  );

  // Phí giao dịch
  $form['market_option']['trading_costs'] = array(
    '#type' => 'textfield',
    '#title' => t('Chi phí giao dịch (%)'),
    '#description' => t('Tính phí mỗi lần thực hiện giao dịch của người đầu tư.'),
    '#default_value' => isset($market_option['trading_costs']) ? $market_option['trading_costs'] : 0.15,
    '#required' => TRUE
  );

  // Biên độ giao dịch
  $form['market_option']['price_range'] = array(
    '#type' => 'textfield',
    '#title' => t('Biên độ giá (%)'),
    '#description' => t('Biên độ giá để tính giá trần và giá sàn.'),
    '#default_value' => isset($market_option['price_range']) ? $market_option['price_range'] : 7,
    '#required' => TRUE
  );

  // Nút save
  $form['actions'] = array('#type' => 'container');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Lưu thay đổi'),
  );

  $form['#submit'][] = 'stock_market_initialize_market_submit';
  $form['#validate'][] = 'stock_market_initialize_market_validate';

  return $form;
}

/**
 * Xử lý khi field mã chứng khoán được chọn
 * Sẽ tạo lại field stock code
 */
function stock_market_initialize_market_stock_code_ajax($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  return $form['stock_code']['stock_code'];
}

/**
 * Khi lựa chọn số phiên giao dịch trong một ngày
 * Sẽ trả lại những field cấu hình thời gian giao dịch cho từng loại khớp lệnh
 */
function stock_market_initialize_market_trading_sessions_ajax($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  return $form['market_option']['trading_sessions'];
}

/**
 * Xử lý và lưu dữ liệu khi khởi tạo thị trường
 */
function stock_market_initialize_market_submit($form, &$form_state) {
  // Trước khi lưu dữ liệu mới cần phải xóa bỏ dữ liệu cũ
  db_update('stock_code')
    ->fields(array('status' => 0, 'reference_price' => 0))
    ->execute();

  // Cập nhật lại dữ liệu cho từng mã chứng khoán
  foreach ($form_state['values']['stock_code'] as $code => $data) {
    if ($data['value'] == 1 && !empty($form_state['stock_codes'][$code])) {
      $stock_code = $form_state['stock_codes'][$code];
      $stock_code->status = 1;
      $stock_code->reference_price = $data['reference_price'];
      stock_code_save($stock_code);
    }
  }

  // Lưu dữ liệu cấu hình thị trường
  if (isset($form_state['values']['market_option'])) {
    $market_option = $form_state['values']['market_option'];
    $market_option['open_days'] = array_filter($market_option['open_days']);
    variable_set('stock_market_market_option', $market_option);
  }
  drupal_set_message('Khởi tạo thị trường thành công.');
}

/**
 * kiểm tra dữ liệu
 */
function stock_market_initialize_market_validate($form, &$form_state) {
  $stock_code_empty = TRUE;
  foreach ($form_state['values']['stock_code'] as $stock_code => $stock_code_value) {
    if ($stock_code_value['value'] == 1) {
      $stock_code_empty = FALSE;
      // Giá tham chiếu phải luôn luôn lớn hơn 1
      if ($stock_code_value['reference_price'] <= 0) {
        form_set_error('stock_code][' . $stock_code . '][reference_price', t('Giá tham chiếu của cố phiếu @stock_code không hợp lệ, giá tham chiếu cần lớn hơn 0', array('@stock_code' => $stock_code)));
      }
    }
  }
  // Báo lỗi khi không có cổ phiếu nào được niêm yết
  if ($stock_code_empty) {
    form_set_error('stock_code', t('Không có cổ phiếu nào được niêm yết. Bạn cần chọn ít nhất 1 mã cổ phiếu trước khi có thể khởi tạo thị trường'));
  }

  // Kiểm tra tính hợp lệ của thời gian giao dịch cho từng phiên
  foreach ($form_state['values']['market_option']['trading_sessions'] as $delta => $trading_session) {
    foreach ($trading_session['trading_type'] as $type => $trading_type_time) {
      $time = new DateObject($trading_type_time, NULL, 'h:i A');
      if (isset($time->errors)) {
        foreach ($time->errors as $error) {
          form_set_error('market_option][trading_sessions][' . $delta . '][trading_type][' . $type, $error);
        }
      }
    }
  }
}

/**
 * Form nhập khẩu mã cổ phiếu từ thị trường thật
 */
function stock_market_real_market_stock_code_import($form, &$form_state) {
  $form['caution'] = array(
    '#markup' => t('<p>Quá trình nhập khẩu có thể sẽ mất thời gian khoảng hơn 1 phút. Tùy thuộc vào tốc độ mạng của hệ thống và tốc độ phản hồi của máy chủ chứa dữ liệu mã chứng khoán. Lưu ý rằng hành động này có thể làm thay đổi dữ liệu mã đang có trên hệ thống</p>'),
  );

  $form['import'] = array(
    '#type' => 'submit',
    '#value' => t('Nhập khẩu'),
    '#submit' => array('stock_market_real_market_stock_code_import_submit')
  );

  return $form;
}

/**
 * Xử lý sự kiện khi nhấp vào nut import
 */
function stock_market_real_market_stock_code_import_submit($form, &$form_state) {
  $batch = array(
    'operations' => array(
      array('stock_market_real_market_stock_code_import_batch_get_stock', array()),
      array('stock_market_real_market_stock_code_import_batch_save_stock', array()),
    ),
    'finished' => 'stock_market_real_market_stock_code_import_batch_finished',
    'title' => t('Nhập khẩu mã chứng khoán từ thị trường thật'),
    'init_message' => t('Bắt đầu nhập khẩu mã chứng khoán.'),
    'progress_message' => t('Đã xử lý @current trong tổng số @total.'),
    'error_message' => t('Có lỗi trong quá trình xử lý dữ liệu.'),
    'file' => drupal_get_path('module', 'stock_market') . '/stock_market.page.admin.inc'
  );

  batch_set($batch);
}

/**
 * Lấy mã chứng khoán từ thị trường thật
 */
function stock_market_real_market_stock_code_import_batch_get_stock(&$context) {
  // Danh sách thông tin của sàn giao dịch chứng khoán trên thị trường thật
  // Bao gồm tên sàn và url để lấy dữ liệu
  $floors = array(
    'UPCOM' => array(
      'url' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=init&getVersion=-1&IndexCode=UPCOMINDEX&catid=3',
      'name' => 'UPCOM'
    ),
    'HOUSE' => array(
      'url' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=init&getVersion=-1&IndexCode=VNINDEX&catid=1',
      'name' => 'HOUSE'
    ),
    'HNX' => array(
      'url' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=init&getVersion=-1&IndexCode=HASTCINDEX&catid=2',
      'name' => 'HNX'
    )
  );

  // Mỗi request sẽ lấy dữ liệu từ 1 sàn
  if (empty($context['sandbox'])) {
    $context['sandbox']['max'] = count($floors);
    $context['sandbox']['current_start_point'] = 0;
  }

  // Lấy thông tin sàn đang được chạy
  $floor_info = array_slice($floors, $context['sandbox']['current_start_point'], 1);
  $floor_info = array_shift($floor_info);
  $context['message'] = t('Đang lấy dữ liệu sàn %floor_name', array('%floor_name' => $floor_info['name']));

  // Gửi request để lấy dữ liệu
  $request = drupal_http_request($floor_info['url']);
  $raw_data = drupal_json_decode($request->data);

  // Nếu không có lỗi sảy ra và dữ liệu đúng thì sẽ lấy thông tin cổ phiếu
  // Nếu dữ liệu lấy về không hợp lệ hoặc trong quá trình gửi request có phát sinh lỗi
  // Thì thực hiện gửi lại request
  if (!empty($raw_data) && is_array($raw_data)) {
    foreach ($raw_data as $stock_code_info) {
      // Dữ liệu sai thì báo lỗi
      if (empty($stock_code_info['ST'])) {
        throw new Exception(t('Dữ liệu không hợp lệ.'));
      }

      // Nếu tên công ty rỗng thì sẽ lấy từ nguồn khác
      if (empty($stock_code_info['SN'])) {
        $search_request = drupal_http_request('http://finance.vietstock.vn/vsSearchBox.ashx?q=' . $stock_code_info['ST']);
        $search_results = explode('|', $search_request->data);
        if ($search_results[0] === $stock_code_info['ST']) {
          $stock_code_info['SN'] = $search_results[1];
        }
        else {
          $stock_code_info['SN'] = t('Đang cập nhật...');
        }
      }

      // Lưu vào danh sách cổ phiếu chờ lưu
      $context['results']['stocks_code'][$stock_code_info['ST']] = array(
        'code' => $stock_code_info['ST'],
        'company' => $stock_code_info['SN']
      );
    }
    $context['sandbox']['current_start_point'] ++;
    $context['finished'] = $context['sandbox']['current_start_point'] >= $context['sandbox']['max'];
  }
  else {
    // Thông báo khi có lỗi xảy ra trong quá trình lấy dữ liệu
    $context['message'] = t('Lỗi dữ liệu trên sàn %floor_name, Đang cố thử lại <br/><code>@error_message</code>.', array(
      '%floor_name' => $floor_info['name'],
      '@error_message' => !empty($request->error) ? $request->error : ''));
    $context['finished'] = FALSE;
  }
}

/**
 * lưu dữ liệu chứng khoán từ stock_market_real_market_stock_code_import_batch_get_stock().
 * 
 * @see stock_market_real_market_stock_code_import_batch_get_stock().
 */
function stock_market_real_market_stock_code_import_batch_save_stock(&$context) {
  // Lưu 20 mã cổ phiếu một lần
  if (empty($context['sandbox'])) {
    $context['sandbox']['max'] = count($context['results']['stocks_code']);
    $context['sandbox']['current_start_point'] = 0;
  }
  $limit = 20;
  $stock_tobe_save = array_slice($context['results']['stocks_code'], $context['sandbox']['current_start_point'], $limit);

  foreach ($stock_tobe_save as $data) {
    // Kiểm tra nếu mã này đã có trên hệ thống thì chỉ cập nhật dữ liệu mới
    // Nếu chưa có thì tạo mới
    if (($stock = stock_code_load($data['code']))) {
      $stock->company_name = $data['company'];
      stock_code_save($stock);
    }
    else {
      $stock = entity_create('stock_code', array());
      $stock->status = 0;
      $stock->reference_price = 0;
      $stock->stock_code = $data['code'];
      $stock->company_name = $data['company'];
      stock_code_save($stock);
    }
  }

  $context['sandbox']['current_start_point'] += $limit;
  $context['finished'] = $context['sandbox']['current_start_point'] >= $context['sandbox']['max'];
  $context['message'] = t('Đã lưu tổng số @stock_processed cổ phiếu trong tổng số @total_stock cố phiếu.', array(
    '@stock_processed' => !$context['finished'] ? $context['sandbox']['current_start_point'] : $context['sandbox']['max'],
    '@total_stock' => $context['sandbox']['max']
  ));
}

/**
 * Xử lý Khi hoàn thành quá trình nhập mã chứng khoán
 */
function stock_market_real_market_stock_code_import_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Đã nhập khẩu mã chứng khoán thành công từ thị trường thật và lưu lại trên hệ thống.'));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('Đã có một vài lỗi xảy ra trong quá trình xử lý, bạn hãy thử thực hiện lại quá trình nhập khẩu thêm lần nữa.'));
  }
}
