<?php

/**
 * Trả về danh sách những loại giao dịch
 */
function stock_market_user_trading_type_list() {
  return array(
    TRADING_TYPE_BUY_STOCK => t('Mua'),
    TRADING_TYPE_SELL_STOCK => t('Bán')
  );
}

/**
 * Trả về danh sách tình trạng của giao dịch
 */
function stock_market_user_status_list() {
  return array(
    TRADING_STATUS_COMPLETED => t('Hoàn thành'),
    TRADING_STATUS_PENDING => t('Chờ thanh toán'),
    TRADING_STATUS_WAIT_MATCHING => t('Chờ khớp'),
  );
}

/**
 * Trả về danh sách loại lệnh
 */
function stock_market_user_trading_command_type() {
  return array(
    'LO' => 'LO',
    'ATO' => 'ATO',
    'ATC' => 'ATC',
    'MP' => 'MP',
  );
}
