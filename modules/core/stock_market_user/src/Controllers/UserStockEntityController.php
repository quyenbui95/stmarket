<?php

namespace Drupal\stock_market_user\Controllers;

class UserStockEntityController extends \EntityAPIController
{

  /**
   * Tạo ra object user_stock
   */
  public function create(array $values = array()) {
    $values = array_merge(array(
      'user_stock_id' => NULL,
      'uid' => NULL,
      'bundle' => 'user_stock',
      'stock_code' => NULL,
      'volumes' => 0
      ), $values);
    return parent::create($values);
  }

  /**
   * Xử lý thêm khi lưu entity
   * - Mỗi user chỉ có thể có 1 bản ghi cho 1 mã cổ phiếu duy nhất
   * - Người dùng cần phải tồn tại
   * - Mã cổ phiếu cũng cần phải tồn tại và đang được cho phép đầu tư
   */
  public function save($entity, \DatabaseTransaction $transaction = NULL) {
    $errors = array();
    if (!empty($entity->is_new) || empty($entity->{$this->idKey})) {
      $user_stock = user_stock_load_by_user_code($entity->stock_code, user_load($entity->uid));
      if ($user_stock) {
        $errors[] = t('User stock: Mỗi user chỉ có thể có 1 bản ghi cho 1 mã cổ phiếu duy nhất.');
      }

      // Người dùng cần phải đang tồn tại
      if (empty($entity->uid) || !user_load($entity->uid)) {
        $errors[] = t('User stock: Người dùng không tồn tại trên hệ thống.');
      }

      // Mã cổ phiếu cũng cần phải tồn tại và đang được cho phép đầu tư
      if (!($stock_code = stock_code_load($entity->stock_code)) || $stock_code->status == 0) {
        $errors[] = t('User stock: Mã cổ phiếu %stock_code_name không tồn tại trên hệ thống hoặc nó chưa được niêm yết trên thị trường.', array(
          '%stock_code_name' => $entity->stock_code,
        ));
      }
    }

    foreach ($errors as $error) {
      watchdog('user stock', $error, array(), WATCHDOG_ERROR);
    }

    // Lưu entity lên database
    if (empty($errors)) {
      return parent::save($entity, $transaction);
    }

    return NULL;
  }

  /**
   * Trừ đi số lượng cổ phiếu của người chơi
   * Nếu số lượng cổ phiếu sau khi đã trừ mà con lại bằng 0 thì cổ phiếu đó sẽ bị xóa
   * Có nghĩa là người chơi không còn sở hữu cổ phiếu đó nữa
   * 
   * @param $account
   *   Object user trả về từ user_load().
   * @param $stock_code
   *   Mã cổ phiếu cần trừ bớt ví dụ: ACB
   * @param $volumes
   *  Tổng số khối lượng cần trừ bớt
   */
  public function subtractVolumes($account, $stock_code, $volumes) {
    $user_stock = user_stock_load_by_user_code($stock_code, $account);
    if ($user_stock && $user_stock->volumes >= $volumes) {
      $subtracted = $user_stock->volumes - $volumes;
      if ($subtracted < 1) {
        user_stock_delete($user_stock);
      }
      else {
        $user_stock->volumes = $subtracted;
        user_stock_save($user_stock);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Cộng thêm số lượng cổ phiếu của người chơi
   * Nếu user chưa sở hữu cổ phiếu thì sẽ tạo ra cổ phiếu cho user
   * 
   * @param $account
   *   Object user trả về từ user_load().
   * @param $stock_code
   *   Mã cổ phiếu cần cộng thêm ví dụ: ACB
   * @param $volumes
   *  Tổng số khối lượng cần cộng thêm
   */
  public function increaseVolumes($account, $stock_code, $volumes) {
    $user_stock = user_stock_load_by_user_code($stock_code, $account);
    $volumes = (int) $volumes;
    if ($user_stock && $volumes > 0) {
      $user_stock->volumes += $volumes;
    }
    elseif ($volumes > 0) {
      // Tạo cổ phiếu cho user
      $user_stock = entity_create('user_stock', array(
        'uid' => $account->uid,
        'stock_code' => $stock_code,
        'volumes' => $volumes
      ));
    }

    if ($user_stock) {
      try {
        user_stock_save($user_stock);
        return TRUE;
      }
      catch (Exception $ex) {
        return FALSE;
      }
    }
  }

}
