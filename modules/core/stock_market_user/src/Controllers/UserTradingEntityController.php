<?php

namespace Drupal\stock_market_user\Controllers;

class UserTradingEntityController extends \EntityAPIController
{

  /**
   * Save entity
   * 
   * @param type $entity
   * @param \DatabaseTransaction $transaction
   */
  public function save($entity, \DatabaseTransaction $transaction = NULL) {
    // Gán giá trị mặc định cho created
    if (!$entity->created) {
      $entity->created = time();
    }

    parent::save($entity, $transaction);
  }

  /**
   * Khi 1 lệnh giao dịch không được khớp lệnh hết mà chỉ khớp 1 phần trong tổng số
   * Khối lượng giao dịch thì sẽ vẫn giữ giao dịch hiện tại là đang chờ khớp lệnh
   * Nhưng trừ đi khối lượng cổ phiếu đã khớp và tạo ra một giao dịch mới
   * Với tình trạng chờ thanh toán
   * 
   * @param $matched_volumns
   *   Tổng khối lượng cổ phiếu khớp lệnh
   * @param $user_trading
   *   Giao dịch
   */
  public function partiallyMatched($matched_volumns, \Drupal\stock_market_user\Entity\UserTradingEntity $user_trading) {
    // Trừ đi 1 phần khối lượng đã khớp của giao dịch
    if ($user_trading->volumes >= $matched_volumns) {
      $user_trading->volumes -= $matched_volumns;

      // Tạo một giao dịch mới
      $user_trading_matched = entity_create('user_trading', array(
        'uid' => $user_trading->uid,
        'created' => $user_trading->created,
        'stock_code' => $user_trading->stock_code,
        'volumes' => $matched_volumns,
        'price' => $user_trading->price,
        'status' => TRADING_STATUS_PENDING,
        'trading_type' => $user_trading->trading_type,
      ));

      // Lưu giao dịch
      try {
        $c1 = $user_trading->save();
        $c2 = $user_trading_matched->save();
        return TRUE;
      }
      catch (Exception $ex) {
        return FALSE;
      }
    }

    return FALSE;
  }

}
