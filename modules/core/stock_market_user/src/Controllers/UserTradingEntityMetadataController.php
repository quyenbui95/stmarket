<?php

namespace Drupal\stock_market_user\Controllers;

class UserTradingEntityMetadataController extends \EntityDefaultMetadataController
{

  public function entityPropertyInfo() {
    return array(
      'user_trading' => array(
        'properties' => array(
          'trading_id' => array(
            'type' => 'integer',
            'label' => 'Giao dịch ID',
            'schema field' => 'trading_id',
            'validation callback' => 'entity_metadata_validate_integer_positive',
            'description' => 'Id của giao dịch.',
          ),
          'uid' => array(
            'type' => 'user',
            'label' => 'Uid',
            'schema field' => 'uid',
            'description' => 'Tài khoản thực hiện giao dịch.',
          ),
          'created' => array(
            'type' => 'integer',
            'label' => 'Created',
            'schema field' => 'created',
            'description' => 'Thời điểm thực hiện giao dịch.',
          ),
          'matched_date' => array(
            'type' => 'integer',
            'label' => 'Matched date',
            'schema field' => 'matched_date',
            'description' => 'Thời điểm khớp lệnh.',
          ),
          'trading_term_date' => array(
            'type' => 'integer',
            'label' => 'Trading term date',
            'schema field' => 'trading_term_date',
            'description' => 'Thời gian hết hạn chờ khớp.',
          ),
          'sessions_left' => array(
            'type' => 'integer',
            'label' => 'Sessions left',
            'schema field' => 'sessions_left',
            'description' => 'Số phiên đợi thanh toán còn lại.',
          ),
          'stock_code' => array(
            'type' => 'stock_code',
            'label' => 'Stock code',
            'schema field' => 'stock_code',
            'description' => 'Mã chứng khoán.',
          ),
          'volumes' => array(
            'type' => 'integer',
            'label' => 'Volumes',
            'schema field' => 'volumes',
            'description' => 'Khối lượng.',
          ),
          'price' => array(
            'type' => 'integer',
            'label' => 'Price',
            'schema field' => 'price',
            'description' => 'Giá tiền.',
          ),
          'command_type' => array(
            'type' => 'text',
            'label' => 'Command type',
            'schema field' => 'command_type',
            'description' => 'Loại khớp lệnh.',
            'options list' => 'stock_market_user_trading_command_type'
          ),
          'status' => array(
            'type' => 'integer',
            'label' => 'Status',
            'schema field' => 'status',
            'description' => 'Tình trạng của giao dịch.',
            'options list' => 'stock_market_user_status_list'
          ),
          'trading_type' => array(
            'type' => 'text',
            'label' => 'Trading_type',
            'schema field' => 'trading_type',
            'description' => 'Loại giao dịch.',
            'options list' => 'stock_market_user_trading_type_list'
          ),
        ),
      ),
      ) + parent::entityPropertyInfo();
  }

}
