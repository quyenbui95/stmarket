<?php

namespace Drupal\stock_market_user\Controllers;

class UserTradingEntityUIController extends \EntityDefaultUIController
{

  public function hook_menu() {
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';

    parent::hook_menu();

    $items[$this->path . '/' . $wildcard . '/delete'] = array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array($this->entityType . '_operation_form', $this->entityType, $this->id_count, 'delete'),
      'load arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('delete', $this->entityType, $this->id_count),
      'title' => 'Delete',
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'file' => isset($this->entityInfo['admin ui']['file']) ? $this->entityInfo['admin ui']['file'] : NULL,
      'file path' => isset($this->entityInfo['admin ui']['file path']) ? $this->entityInfo['admin ui']['file path'] : drupal_get_path('module', $this->entityInfo['module']),
    );

    return $items;
  }

  public function operationForm($form, &$form_state, $entity, $op) {
    switch ($op) {
      case 'delete':
        $wrapper = entity_metadata_wrapper('user_trading', $entity);
        $confirm_question = t('Bạn có muốn hủy đặt lệnh %trading_type cổ phiếu %stock_code?', array('%trading_type' => $wrapper->trading_type->label(), '%stock_code' => $entity->stock_code));
        return confirm_form($form, $confirm_question, $this->path, t('Hành động này sẽ xóa lệnh đã chọn vĩnh viễn khỏi hệ thống và sẽ không thể khôi phục lại.'));
    }
  }

}
