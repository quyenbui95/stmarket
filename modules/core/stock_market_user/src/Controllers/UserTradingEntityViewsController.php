<?php

namespace Drupal\stock_market_user\Controllers;

class UserTradingEntityViewsController extends \EntityDefaultViewsController
{

  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();

    if (!empty($this->info['base table'])) {
      $table = $this->info['base table'];
      if (isset($data[$table])) {
        $data[$table]['delete_path'] = array(
          'field' => array(
            'title' => t('User trading delete path'),
            'help' => t('The path to delete the user trading.'),
            'handler' => 'Drupal\stock_market_user\Views\Handlers\FieldUserTradingDeletePath',
          ),
        );
      }
    }

    return $data;
  }

}
