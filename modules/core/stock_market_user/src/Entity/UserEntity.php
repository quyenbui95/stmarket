<?php

namespace Drupal\stock_market_user\Entity;

class UserEntity extends \Entity
{

  /**
   * Lấy ra số tiền mà user đang có trong tài khoản
   */
  public function getMoney() {
    if (!empty($this->field_user_money['und'][0]['value']) && $this->field_user_money['und'][0]['value'] > 0) {
      return $this->field_user_money['und'][0]['value'];
    }
    else {
      return 0;
    }
  }

  /**
   * Chỉ định cho người dùng một số tiền mới
   * 
   * @param $money
   *   Số tiền sẽ được chỉ định cho người dùng
   */
  public function setMoney($money) {
    $this->field_user_money['und'][0]['value'] = $money;

    return TRUE;
  }

  /**
   * Cộng thêm tiền cho người dùng
   * 
   * @param $money
   *   Số tiền sẽ được cộng thêm
   * @return
   *   Luôn luôn trả về TRUE
   */
  public function increaseMoney($money) {
    $this->setMoney($this->getMoney() + $money);

    return TRUE;
  }

  /**
   * Trừ tiền từ tài khỏan của người dùng
   * 
   * @param $money
   *   Số tiền sẽ trừ từ tài khoản của người dùng
   * @return
   *   Trả về TRUE nếu có thể trừ
   *   Trả về FALSE nếu số tiền đang có trong tài khoản không đủ để trừ
   */
  public function subtractMoney($money) {
    $current_money = $this->getMoney();
    if ($current_money >= $money) {
      $this->setMoney($current_money - $money);

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Lấy thông tin người dùng
   */
  public function getUserProfile() {
    return profile2_load_by_user($this, 'personal_information');
  }

  /**
   * Lấy ra toàn bộ số cổ phiếu mà người dùng này đang nắm giữ
   */
  public function getStocks() {
    return user_stock_load_by_user($this);
  }

  /**
   * Lấy ra một mã cố phiếu đang được người dùng này nắm giữ
   */
  public function getStock($stock_code) {
    return user_stock_load_by_user_code($stock_code, $this);
  }

  /**
   * Lưu user
   */
  public function save() {
    user_save($this);
  }

  /**
   * Kiểm tra thành viên có phải là nhà đầu tư hay không
   */
  public function isInvestors() {
    return in_array(USER_INVESTORS_ROLES, $this->roles);
  }

  /**
   * Kiểm tra user có phải là super admin
   */
  public function isSupperAdministrator() {
    return (int) $this->uid === 1;
  }

}
