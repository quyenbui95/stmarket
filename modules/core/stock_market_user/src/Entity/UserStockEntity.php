<?php

namespace Drupal\stock_market_user\Entity;

class UserStockEntity extends \Entity
{

  /**
   * @see Drupal\stock_market_user\Controllers\UserStockEntityController::subtractVolumes().
   */
  public function subtractVolumes($volumes) {
    if ($this->user_stock_id) {
      $account = user_load($this->uid);
      if ($account) {
        $controller = entity_get_controller($this->entityType());
        $controller->subtractVolumes($account, $this->stock_code, $volumes);
      }
    }
  }

  /**
   *  @see Drupal\stock_market_user\Controllers\UserStockEntityController::increaseVolumes().
   */
  public function increaseVolumes($volumes) {
    if ($this->user_stock_id) {
      $account = user_load($this->uid);
      if ($account) {
        $controller = entity_get_controller($this->entityType());
        $controller->increaseVolumes($account, $this->stock_code, $volumes);
      }
    }
  }

}
