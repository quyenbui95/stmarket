<?php

namespace Drupal\stock_market_user\Entity;

class UserTradingEntity extends \Entity
{

  /**
   * Tình trạng giao dịch đang đặt và chờ khớp lệnh
   */
  public function setStatusWaitMatching() {
    $this->status = TRADING_STATUS_WAIT_MATCHING;
  }

  /**
   * Giao dịch đã khớp đang chờ để được thanh toán
   */
  public function setStatusPending() {
    $this->status = TRADING_STATUS_PENDING;
  }

  /**
   * Giao dịch đã hoàn thành
   * + Đã khớp lệnh
   * + Đã thanh toán
   */
  public function setStatusCompleted() {
    $this->status = TRADING_STATUS_COMPLETED;
  }

  /**
   * @see \Drupal\stock_market_user\Controllers\UserTradingEntityController::partiallyMatched(). 
   * @param $matched_volumns
   *   Khối lượng cổ phiếu cần khớp lệnh
   */
  public function partiallyMatched($matched_volumns) {
    if (isset($this->trading_id) && $this->trading_id) {
      $controller = entity_get_controller('user_trading');
      return $controller->partiallyMatched($matched_volumns, $this);
    }

    return FALSE;
  }

}
