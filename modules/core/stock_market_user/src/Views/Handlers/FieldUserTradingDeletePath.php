<?php

namespace Drupal\stock_market_user\Views\Handlers;

class FieldUserTradingDeletePath extends \views_handler_field_node_link
{

  function render_link($user_trading, $values) {
    $path = "stmarket/investors/trading/$user_trading->trading_id/delete";
    if (($menu_item = menu_get_item($path)) && $menu_item['access']) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $path;
      $this->options['alter']['query'] = drupal_get_destination();
      $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
      return $text;
    }
  }

}
