<?php
/**
 * @file
 * stock_market_user.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stock_market_user_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_profile2_type().
 */
function stock_market_user_default_profile2_type() {
  $items = array();
  $items['personal_information'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "personal_information",
    "label" : "Th\\u00f4ng tin c\\u00e1 nh\\u00e2n",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
