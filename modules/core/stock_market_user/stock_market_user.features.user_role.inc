<?php
/**
 * @file
 * stock_market_user.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function stock_market_user_user_default_roles() {
  $roles = array();

  // Exported role: investors.
  $roles['investors'] = array(
    'name' => 'investors',
    'weight' => 3,
  );

  return $roles;
}
