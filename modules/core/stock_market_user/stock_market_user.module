<?php

include_once 'stock_market_user.features.inc';
include_once 'includes/stock_market_user.callback.inc';

/**
 * Role admin
 */
define('USER_ADMINISTRATOR_ROLES', 'administrator');

/**
 * Role nhà đầu tư
 */
define('USER_INVESTORS_ROLES', 'investors');

/**
 * Kí tự đại diện cho giao dịch đang chờ khớp lệnh
 */
define('TRADING_STATUS_WAIT_MATCHING', 0);

/**
 * Kí tự đại diện cho giao dịch đang chờ thanh toán
 */
define('TRADING_STATUS_PENDING', 1);

/**
 * Giao dịch đã thanh toán
 */
define('TRADING_STATUS_COMPLETED', 2);

/**
 * Kí tự đại diện cho giao dịch bán cổ phiếu
 */
define('TRADING_TYPE_SELL_STOCK', 'S');

/**
 * Kí tự đại diện cho giao dịch mua cổ phiếu
 */
define('TRADING_TYPE_BUY_STOCK', 'B');

/**
 * Implements hook_entity_info().
 */
function stock_market_user_entity_info() {
  // Entity lưu tài khoản cổ phiếu của người chơi
  $info['user_stock'] = array(
    'label' => t('Cổ phiếu của nhà đầu tư'),
    'controller class' => 'Drupal\stock_market_user\Controllers\UserStockEntityController',
    'entity class' => 'Drupal\stock_market_user\Entity\UserStockEntity',
    'base table' => 'user_stock',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'user_stock_id',
      'label' => 'stock_code',
    ),
    'bundles' => array(
      'user_stock' => array(
        'label' => t('Cổ phiếu của nhà đầu tư')
      ),
    ),
    'static cache' => TRUE,
    'module' => 'stock_market_user',
  );

  // Entity lưu trữ những giao dịch của thành viên
  $info['user_trading'] = array(
    'label' => t('Giao dịch'),
    'controller class' => 'Drupal\stock_market_user\Controllers\UserTradingEntityController',
    'entity class' => 'Drupal\stock_market_user\Entity\UserTradingEntity',
    'base table' => 'user_trading',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'trading_id',
      'label' => 'stock_code',
    ),
    'bundles' => array(
      'user_trading' => array(
        'label' => t('Giao dịch')
      ),
    ),
    'access callback' => 'stock_market_user_user_trading_management_access',
    'admin ui' => array(
      'path' => 'stmarket/investors/trading',
      'controller class' => 'Drupal\stock_market_user\Controllers\UserTradingEntityUIController',
    ),
    'static cache' => TRUE,
    'module' => 'stock_market_user',
    'metadata controller class' => 'Drupal\stock_market_user\Controllers\UserTradingEntityMetadataController',
    'views controller class' => 'Drupal\stock_market_user\Controllers\UserTradingEntityViewsController',
  );

  return $info;
}

/**
 * Kiểm tra quyền CRUD giao dịch
 * - Chỉ có chính nhà đầu tư có thể delete cổ phiếu của họ và chỉ có thể delete những lệnh đang chờ khớp
 * - Chỉ có supper admin mới có thể edit/delete 1 giao dịch của bất kì nhà đầu tư nào
 * 
 * @param type $op
 * @param type $user_trading
 * @param type $account
 * @return boolean
 */
function stock_market_user_user_trading_management_access($op, $user_trading = NULL, $account = NULL) {
  global $user;

  $account = $account ? $account : user_load($user->uid);

  if ($account->isSupperAdministrator()) {
    return TRUE;
  }

  if (in_array($op, array('delete'))) {
    return $user_trading->uid == $account->uid && $user_trading->status == TRADING_STATUS_WAIT_MATCHING;
  }

  return FALSE;
}

/**
 * Implements hook_entity_info_alter().
 */
function stock_market_user_entity_info_alter(&$entity_info) {
  // Sử dụng controller class của chúng tôi khai báo cho entity user
  $entity_info['user']['controller class'] = 'Drupal\stock_market_user\Controllers\UserController';
  // Sử dụng entity class của chúng tôi khai báo cho entity user
  $entity_info['user']['entity class'] = 'Drupal\stock_market_user\Entity\UserEntity';
  // Khi tạo mới object user sẽ sử dụng class của chúng tôi thay vì stdClass
  unset($entity_info['user']['creation callback']);
  // Ghi đè metadata controller bởi vì module entity đã tạo sẵn property cho core entity
  // Sửa lỗi bị trùng lặp property
  $entity_info['user']['metadata controller class'] = 'Drupal\stock_market_user\Controllers\UserEntityMetadataController';
}

/**
 * Implements hook_module_implements_alter().
 */
function stock_market_user_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'entity_info_alter') {
    // Di chuyển hook entity_info_alter mà chúng tôi đã implement xuống dưới cùng
    // Hook của tôi cần phải chạy sau cùng để ghi đè lên module entity
    $group = $implementations['stock_market_user'];
    unset($implementations['stock_market_user']);
    $implementations['stock_market_user'] = $group;
  }
}

/**
 * Load nhiều mã chứng khoán của người dùng dựa vào id
 */
function user_stock_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('user_stock', $ids, $conditions, $reset);
}

/**
 * Lấy ra dữ liệu của một mã cổ phiếu của một người dùng
 *
 * @param $id
 *   ID user_stock
 * @param $reset
 *   Whether to reset the stock_code_load_multiple cache.
 */
function user_stock_load($id = NULL, $reset = FALSE) {
  $ids = (isset($id) ? array($id) : array());
  $user_stock = user_stock_load_multiple($ids, NULL, $reset);
  return $user_stock ? reset($user_stock) : FALSE;
}

/**
 * Lấy dữ liệu chứng khoán của một người chơi dựa theo mã cổ phiếu
 * 
 * @param $stock_code
 *   Mã chứng khoán
 * @param $account
 *   Object user
 */
function user_stock_load_by_user_code($stock_code, $account) {
  $id = db_select('user_stock')
      ->fields('user_stock', array('user_stock_id'))
      ->condition('uid', $account->uid)
      ->condition('stock_code', $stock_code)
      ->execute()->fetchField();
  return $id ? user_stock_load($id) : NULL;
}

/**
 * Lấy toàn bộ cổ phiếu của user
 * 
 * @param $account
 *   Object user
 */
function user_stock_load_by_user($account) {
  $result = db_select('user_stock')
      ->fields('user_stock', array('user_stock_id'))
      ->condition('uid', $account->uid)
      ->execute()->fetchAll();
  $ids = array();
  foreach ($result as $id) {
    $ids[$id->user_stock_id] = $id->user_stock_id;
  }

  return $ids ? user_stock_load_multiple($ids) : NULL;
}

/**
 * Lấy toàn bộ theo mã cổ phiếu
 * 
 * @param $stock_code
 *   Tên mã cổ phiếu
 */
function user_stock_load_by_code($stock_code) {
  $result = db_select('user_stock')
      ->fields('user_stock', array('user_stock_id'))
      ->condition('stock_code', $stock_code)
      ->execute()->fetchAll();
  $ids = array();
  foreach ($result as $id) {
    $ids[$id->user_stock_id] = $id->user_stock_id;
  }

  return $ids ? user_stock_load_multiple($ids) : NULL;
}

/**
 * Lưu mã chứng khoán
 * 
 * @param $user_stock
 *   Object user_stock
 * @see user_stock_load().
 */
function user_stock_save($user_stock) {
  return entity_save('user_stock', $user_stock);
}

/**
 * Xóa user_stock
 * 
 * @param $user_stock
 *   user_stock entity Object
 */
function user_stock_delete($user_stock) {
  return user_stock_delete_multiple(array($user_stock));
}

/**
 * Xóa nhiều user_stock
 * 
 * @param $user_stocks
 *   Mảng những user_stock cần xóa bỏ
 */
function user_stock_delete_multiple($user_stocks) {
  $ids = array();
  foreach ($user_stocks as $user_stock) {
    $ids[$user_stock->user_stock_id] = $user_stock->user_stock_id;
  }
  return entity_delete_multiple('user_stock', $ids);
}

/**
 * Lấy ra một giao dịch thông qua trading_id
 *
 * @param $trading_id
 *   ID của giao dịch cần lấy
 * @param $reset
 *   Whether to reset the stock_code_load_multiple cache.
 */
function user_trading_load($trading_id = NULL, $reset = FALSE) {
  $ids = (isset($trading_id) ? array($trading_id) : array());
  $user_trading = user_trading_load_multiple($ids, NULL, $reset);
  return $user_trading ? reset($user_trading) : FALSE;
}

/**
 * Load nhiều mã chứng khoán của người dùng dựa vào id
 */
function user_trading_load_multiple($trading_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('user_trading', $trading_ids, $conditions, $reset);
}

/**
 * Lưu giao dịch
 * 
 * @param $user_trading
 *   Object user_trading return from user_trading_load().
 * @see user_trading_load().
 */
function user_trading_save(Drupal\stock_market_user\Entity\UserTradingEntity $user_trading) {
  return entity_save('user_trading', $user_trading);
}

/**
 * Xóa giao dịch
 * 
 * @param $user_trading
 *   Object user_trading return from user_trading_load().
 */
function user_trading_delete($user_trading) {
  return user_trading_delete_multiple(array($user_trading));
}

/**
 * Xóa nhiều giao dịch
 * 
 * @param $user_tradings
 *   Mảng những giao dịch cần xóa
 * @see user_trading_load().
 */
function user_trading_delete_multiple(array $user_tradings) {
  $ids = array();
  foreach ($user_tradings as $user_trading) {
    if ($user_trading instanceof Drupal\stock_market_user\Entity\UserTradingEntity) {
      $ids[$user_trading->trading_id] = $user_trading->trading_id;
    }
  }
  return entity_delete_multiple('user_trading', $ids);
}

/**
 * Implements hook_entity_delete().
 */
function stock_market_user_entity_delete($entity, $type) {
  // Khi người dùng bị xóa khỏi hệ thống thì những mã chứng khoán do người dùng
  // Đó đang sở hữu cũng sẽ bị xóa
  // Tương tự khi mã chứng khoán bị xóa khỏi hệ thống thì mã chứng khoán mà
  // người dùng đang sở hữu cũng bị xóa
  if (in_array($type, array('user', 'stock_code'))) {
    if ($type === 'user') {
      $user_stocks = user_stock_load_by_user($entity);
    }
    elseif ($type === 'stock_code') {
      $user_stocks = user_stock_load_by_code($entity->stock_code);
    }

    user_stock_delete_multiple($user_stocks);
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function stock_market_user_stock_code_update($entity) {
  // Khi một mã cổ phiếu bị hủy niêm yết trên thị trường
  // Thì toàn bộ những cổ phiếu đang nắm giữ bởi một user bất kỳ sẽ bị xóa
  if ($entity->status == 0) {
    $user_stocks = user_stock_load_by_code($entity->stock_code);
    user_stock_delete_multiple($user_stocks);
  }
}

/**
 * Implements hook_menu().
 */
function stock_market_user_menu() {
  $items = array();
  $items['stmarket/investors/stock-holding'] = array(
    'title' => 'Cổ phiếu đang sở hữu',
    'page callback' => 'stock_market_user_owned_stock_view',
    'access callback' => 'stock_market_user_stock_access',
    'file' => 'stock_market_user.pages.inc',
  );

  $items['stmarket/investors/trading'] = array(
    'title' => 'Giao dịch cổ phiếu',
    'page callback' => 'stock_market_user_trading_stock_page',
    'access callback' => 'stock_market_user_stock_access',
    'file' => 'stock_market_user.pages.inc',
  );

  $items['stmarket/stock-code/autocomplete/%'] = array(
    'title' => 'Stock code autocomplete',
    'page callback' => 'stock_market_user_stock_code_autocomplete',
    'page arguments' => array(3),
    'access callback' => array('stock_market_user_stock_access'),
    'type' => MENU_CALLBACK,
    'file' => 'stock_market_user.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function stock_market_user_form_user_profile_form_alter(&$form, $form_state) {
  global $user;
  $account = user_load($user->uid);
  $form['field_user_money']['#access'] = $account->isSupperAdministrator();
}

/**
 * Chỉ có người đầu tư mới có thể truy cập trang đầu tư
 */
function stock_market_user_stock_access(Drupal\stock_market_user\Entity\UserEntity $account = NULL) {
  if (!$account) {
    global $user;
    $account = user_load($user->uid);
  }

  return $account->isInvestors();
}

/**
 * Implements hook_theme().
 */
function stock_market_user_theme() {
  return array(
    'stock_teaser_overview' => array(
      'template' => 'templates/stock-teaser-overview',
      'file' => 'stock_market_user.theme.inc',
      'variables' => array(
        'stock_code' => NULL
      ),
    ),
  );
}
