<?php

/**
 * Trang hiển thị toàn bộ cổ phiếu mà người dùng hiện tại đang đăng nhập đang sở hữu
 */
function stock_market_user_owned_stock_view() {
  global $user;
  $header = array(
    array('data' => t('Mã chứng khoán'), 'sort' => 'asc', 'field' => 'stock_code'),
    t('Công ty'),
    array('data' => t('Khối lượng'), 'field' => 'volumes'),
  );

  $sort_info = tablesort_init($header);
  $rows = array();

  // Lấy toàn bộ cổ phiếu của nhà đầu tư
  $user_stocks = user_stock_load_by_user($user);
  if ($user_stocks) {
    foreach ($user_stocks as $user_stock) {
      $stock_code = stock_code_load($user_stock->stock_code);
      $rows[] = array(
        'stock_code' => $stock_code->stock_code,
        $stock_code->company_name,
        'volumes' => $user_stock->volumes
      );
    }
  }

  // Sắp xếp dữ liệu
  usort($rows, function($a, $b) use ($sort_info) {
    if ($sort_info['sort'] === 'asc') {
      return $a[$sort_info['sql']] > $b[$sort_info['sql']];
    }
    else {
      return $a[$sort_info['sql']] < $b[$sort_info['sql']];
    }
  });

  return array(array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('Bạn không có cổ phiếu.'),
      '#sticky' => TRUE,
    ),
    array(
      '#markup' => t('Nhấp lên tiêu đề của cột trong bảng để sắp xếp')
  ));
}

/**
 * Trang giao dịch cổ phiếu
 */
function stock_market_user_trading_stock_page() {
  $content = array();

  // Form
  $content['trading_form'] = drupal_get_form('stock_market_user_trading_stock');

  // Danh sách giao dịch
  $content['trading_list'] = array(
    '#type' => 'fieldset',
    '#title' => 'Giao dịch',
    'list' => array('#markup' => views_embed_view('user_trading_investor_trading_list_filter', 'default'))
  );

  return $content;
}

/**
 * Form mua bán cổ phiếu
 */
function stock_market_user_trading_stock($form, &$form_state) {
  // Loại giao dịch (Bán/Mua)
  if (isset($form_state['clicked_button']['#button_type'])) {
    $form_state['stmarket']['order_type'] = $form_state['clicked_button']['#button_type'];
  }
  $order_type = isset($form_state['stmarket']['order_type']) ? $form_state['stmarket']['order_type'] : NULL;
  $is_trading_sell = $order_type === 'sell_stock';

  $form['#prefix'] = '<div id="stock-market-user-trading-stock-wrapper">';
  $form['#suffix'] = '</div>';

  $form['trading_buttons'] = array(
    '#type' => 'fieldset',
    '#description' => t('Bạn hãy nhấp vào những nút dưới đây để thực hiện giao dịch'),
    '#title' => 'Lệnh giao dịch',
    '#tree' => TRUE
  );

  // Nút mua cổ phiếu
  $form['trading_buttons']['buy_stock'] = array(
    '#type' => 'button',
    '#value' => t('Mua cổ phiếu'),
    '#button_type' => 'buy_stock',
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'stock_market_user_trading_stock_form_ajax',
      'wrapper' => 'stock-market-user-trading-stock-wrapper',
    )
  );

  // Nút bán cổ phiếu
  $form['trading_buttons']['sell_stock'] = array(
    '#type' => 'button',
    '#value' => t('Bán cổ phiếu'),
    '#button_type' => 'sell_stock',
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'stock_market_user_trading_stock_form_ajax',
      'wrapper' => 'stock-market-user-trading-stock-wrapper',
    )
  );

  // Nhập dữ liệu cổ phiếu
  if (isset($order_type) && in_array($order_type, array('buy_stock', 'sell_stock'))) {
    $order_type_label = $is_trading_sell ? t('bán') : t('mua');

    $form['trading_information'] = array(
      '#type' => 'fieldset',
      '#description' => t('Bạn đang thực hiện giao dịch %order_type cổ phiếu', array('%order_type' => $order_type_label)),
      '#title' => t('Đặt lệnh'),
      '#tree' => TRUE
    );

    // Mã chứng khoán cần mua/bán
    $form['trading_information']['stock_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Mã chứng khoán'),
      '#required' => TRUE,
      // Nếu là giao dịch mua cổ phiếu thì sẽ được phép nhập vào toàn bộ mã cổ phiếu đang niêm yết
      // Nếu là giao dịch bán thì chỉ được chọn những cổ phiếu mà user đang có
      '#autocomplete_path' => $is_trading_sell ? 'stmarket/stock-code/autocomplete/holding' : 'stmarket/stock-code/autocomplete/available',
      '#description' => $is_trading_sell ? t('Chỉ chấp nhận những cổ phiếu bạn đang sở hữu') : t('Chỉ chấp nhận những cổ phiếu đang được niêm yết'),
      // Hiện thị thông tin của cổ phiếu khi người chơi đã nhập mã
      '#ajax' => array(
        'callback' => 'stock_market_user_trading_stock_form_ajax',
        'wrapper' => 'stock-market-user-trading-stock-wrapper',
      )
    );

    // Khối lượng cổ phiếu
    $form['trading_information']['volumes'] = array(
      '#type' => 'textfield',
      '#title' => t('Khối lượng'),
      '#required' => TRUE,
      '#description' => t('Khối lượng cổ phiếu bạn muốn') . ' ' . $order_type_label,
    );

    // Giá mua/bán
    $form['trading_information']['command_type'] = array(
      '#type' => 'select',
      '#title' => t('Loại lệnh'),
      '#required' => TRUE,
      '#options' => stock_market_user_trading_command_type(),
      '#ajax' => array(
        'callback' => 'stock_market_user_trading_stock_form_ajax',
        'wrapper' => 'stock-market-user-trading-stock-wrapper',
      ),
    );

    // Giá mua/bán
    $form['trading_information']['price'] = array(
      '#type' => 'textfield',
      '#title' => t('Giá') . ' ' . $order_type_label . ' ( x 1000 )',
      '#required' => TRUE,
      '#description' => t('Giá bạn sẽ trả cho mỗi một cổ phiếu'),
      '#disabled' => !empty($form_state['values']['trading_information']['command_type']) && $form_state['values']['trading_information']['command_type'] !== 'LO'
    );

    // Hiệu lực của lệnh
    $form['trading_information']['trading_term'] = array(
      '#type' => 'select',
      '#title' => t('Thời gian hiệu lực'),
      '#required' => TRUE,
      '#options' => array(
        'today' => 'Trong ngày',
        'many_days' => 'Nhiều ngày'
      ),
      '#default_value' => 'today',
      '#ajax' => array(
        'callback' => 'stock_market_user_trading_stock_form_ajax',
        'wrapper' => 'stock-market-user-trading-stock-wrapper',
      ),
      '#description' => 'Thời gian chờ khớp lệnh'
    );

    // Nhập thời gian sẽ hủy bỏ lệnh nếu chưa được khớp
    if (isset($form_state['values']['trading_information']['trading_term']) && $form_state['values']['trading_information']['trading_term'] === 'many_days') {
      $form['trading_information']['trading_term_date'] = array(
        '#type' => 'textfield',
        '#title' => 'Ngày hết hạn',
        '#required' => TRUE,
        '#description' => t('Thời gian hủy lệnh này nếu vẫn chưa được khớp. Định dạng ngày/tháng/năm ví dụ: %example_date_format', array('%example_date_format' => date('d/m/Y'))),
      );
    }

    $form['trading_information']['actions'] = array(
      '#type' => 'actions'
    );
    $form['trading_information']['actions']['send_trading'] = array(
      '#type' => 'submit',
      '#value' => 'Đặt lệnh',
      '#ajax' => array(
        'callback' => 'stock_market_user_trading_stock_form_ajax',
        'wrapper' => 'stock-market-user-trading-stock-wrapper',
      )
    );

    // Hiển thị thông tin của mã cổ phiếu đang thực hiện giao dịch
    if (!empty($form_state['values']['trading_information']['stock_code'])) {
      $stock_code = stock_code_load($form_state['values']['trading_information']['stock_code']);

      if ($stock_code) {
        $form['trading_stock_code_overview'] = array(
          '#type' => 'fieldset',
          '#title' => t('Mã cổ phiếu %stock_code', array('%stock_code' => $stock_code->stock_code)),
          '#tree' => TRUE,
        );
        $form['trading_stock_code_overview']['overview'] = array(
          '#theme' => 'stock_teaser_overview',
          '#stock_code' => $stock_code
        );
      }
    }
  }

  $form['#validate'] = array('stock_market_user_trading_stock_validate');
  $form['#submit'] = array('stock_market_user_trading_stock_submit');

  return $form;
}

/**
 * @see stock_market_user_trading_stock().
 */
function stock_market_user_trading_stock_form_ajax($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  if (isset($form_state['finished']) && $form_state['finished']) {
    ctools_include('ajax');
    ctools_add_js('ajax-responder');
    return array('#type' => 'ajax', '#commands' => array(ctools_ajax_command_reload()));
  }

  if (isset($form_state['triggering_element']['#limit_validation_errors']) && is_array($form_state['triggering_element']['#limit_validation_errors'])) {
    $form = empty($form_state['triggering_element']['#limit_validation_errors']) ? drupal_rebuild_form('stock_market_user_trading_stock', $form_state) : $form;
  }

  return $form;
}

/**
 * @see stock_market_user_trading_stock().
 */
function stock_market_user_trading_stock_validate($form, &$form_state) {
  global $user;
  $order_type = isset($form_state['stmarket']['order_type']) ? $form_state['stmarket']['order_type'] : NULL;

  if (in_array($order_type, array('buy_stock', 'sell_stock'))) {
    $stock_code = stock_code_load($form_state['values']['trading_information']['stock_code']);
    $is_sell_trading = $order_type === 'sell_stock';
    if ($stock_code) {
      // Mã cổ phiếu cần phải đang được phép đầu tư
      if ($stock_code->status === 0) {
        form_set_error('trading_information][stock_code', t('Mã cổ phiếu %stock_code chưa được niêm yết hoặc không được phép đầu tư tại thời điểm này.', array('%stock_code' => $form_state['values']['trading_information']['stock_code'])));
      }

      // Đối với loại giao dịch bán thì cổ phiếu giao bán người dùng cần phải có nhiều hơn hoặc bằng khối lượng giao bán
      if ($is_sell_trading) {
        $user_stock = user_stock_load_by_user_code($stock_code->stock_code, $user);
        if (!$user_stock || $user_stock->volumes < $form_state['values']['trading_information']['volumes']) {
          form_set_error('trading_information][volumes', t('Bạn không có đủ khối lượng cổ phiếu để bán.'));
        }
      }

      // Giá cổ phiếu cần phải nằm trong khoảng giá trần và sàn
      if (isset($form['trading_information']['price']['#disabled']) && !$form['trading_information']['price']['#disabled']) {
        $price = $form_state['values']['trading_information']['price'];
        if ($price > $stock_code->getCeilPrice() || $price < $stock_code->getFloorPrice()) {
          form_set_error('trading_information][price', t('Giá cần phải nằm trong khoảng giá trần và sàn.'));
        }
      }

      // Xử lý ngày kết thúc của lệnh
      // Ngày kết thúc lệnh, đối với lệnh 1 ngày sẽ tự hủy lệnh vào cuối ngày giao dịch
      if ($form_state['values']['trading_information']['trading_term'] === 'many_days') {
        $end_date = new DateObject($form_state['values']['trading_information']['trading_term_date'], NULL, 'd/m/Y');
        if (!empty($end_date->errors)) {
          foreach ($end_date->errors as $error) {
            form_set_error('trading_information][trading_term_date', $error);
          }
        }
        else {
          $form_state['values']['trading_information']['trading_term_date'] = $end_date->getTimestamp();
        }
      }
    }
    else {
      form_set_error('trading_information][stock_code', t('Cổ phiếu %stock_code không hợp lệ.', array('%stock_code' => $form_state['values']['trading_information']['stock_code'])));
    }
  }
  else {
    form_set_error('order_type', t('Giao dịch của bạn không hợp lệ hệ thống chỉ chấp nhận giao dịch mua/bán.'));
  }
}

/**
 * @see stock_market_user_trading_stock().
 */
function stock_market_user_trading_stock_submit($form, &$form_state) {
  global $user;
  $order_type = $form_state['stmarket']['order_type'];
  $is_sell_trading = $order_type === 'sell_stock';

  // Tạo mới entity
  $new_user_trading = entity_create('user_trading', array(
    'stock_code' => $form_state['values']['trading_information']['stock_code'],
    'uid' => $user->uid,
    'created' => time(),
    'status' => TRADING_STATUS_WAIT_MATCHING,
    'trading_term_date' => $form_state['values']['trading_information']['trading_term_date'],
    'volumes' => $form_state['values']['trading_information']['volumes'],
    'price' => $form_state['values']['trading_information']['price'],
    'command_type' => $form_state['values']['trading_information']['command_type'],
    'trading_type' => $is_sell_trading ? TRADING_TYPE_SELL_STOCK : TRADING_TYPE_BUY_STOCK,
    'sessions_left' => stock_market_get_market_settings('settlement_period')
  ));
  // Lưu entity
  user_trading_save($new_user_trading);

  drupal_set_message('Đặt lệnh thành công.');

  $form_state['finished'] = TRUE;
}

/**
 * Autocomplete cho field stock code
 * - Page callback in stock_market_user_menu().
 */
function stock_market_user_stock_code_autocomplete($type = 'available', $string = '') {
  global $user;

  $matches = array();
  if ($string) {
    // mặc định $type sẽ là avilable, Sẽ lấy toàn bộ mã cổ phiếu đang niêm yết trong hệ thống
    if ($type === 'available') {
      $query = db_select('stock_code');
      $query->fields('stock_code', array('stock_code', 'company_name'));
      $query->condition('stock_code', db_like($string) . '%', 'LIKE');
      $query->condition('status', 1);
    }
    // Đối với $type là holding thì sẽ chỉ lấy những mã cổ phiếu mà người dùng hiện tại đang sở hữu
    elseif ($type === 'holding') {
      $query = db_select('user_stock');
      $query->fields('stock_code', array('stock_code', 'company_name'));
      $query->innerJoin('stock_code', 'stock_code', "stock_code.stock_code = user_stock.stock_code AND stock_code.status = 1");
      $query->condition('uid', $user->uid);
    }

    foreach ($query->execute()->fetchAll() as $result) {
      $matches[$result->stock_code] = '<b style="margin-right: 15px;">' . $result->stock_code . ':</b>' . $result->company_name;
    }
  }

  drupal_json_output($matches);
}
