<?php

/**
 * preprocess for stock_teaser_overview theme
 */
function template_preprocess_stock_teaser_overview(&$variables) {
  $stock_code = $variables['stock_code'];
  if ($stock_code instanceof Drupal\stock_market\Entity\StockCodeEntity) {
    // Tên công ty
    $variables['company_name'] = $stock_code->company_name . ' (' . $stock_code->stock_code . ')';
    // Giá trần
    $variables['ceil_price'] = $stock_code->getCeilPrice();
    // Giá tham chiếu
    $variables['reference_price'] = $stock_code->getReferencePrice();
    // Giá sàn
    $variables['floor_price'] = $stock_code->getFloorPrice();
  }
}
