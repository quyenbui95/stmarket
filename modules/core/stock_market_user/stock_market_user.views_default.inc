<?php
/**
 * @file
 * stock_market_user.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stock_market_user_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'user_trading_investor_trading_list_filter';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'user_trading';
  $view->human_name = 'User trading investor trading list filter';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User trading investor trading list filter';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Tìm kiếm';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'stock_code' => 'stock_code',
    'company_name' => 'company_name',
    'volumes' => 'volumes',
    'price' => 'price',
    'status' => 'status',
    'command_type' => 'command_type',
    'trading_type' => 'trading_type',
    'delete_path' => 'delete_path',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'stock_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'company_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'volumes' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'price' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'command_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'trading_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete_path' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
  );
  /* Relationship: Giao dịch: Stock code */
  $handler->display->display_options['relationships']['stock_code']['id'] = 'stock_code';
  $handler->display->display_options['relationships']['stock_code']['table'] = 'user_trading';
  $handler->display->display_options['relationships']['stock_code']['field'] = 'stock_code';
  $handler->display->display_options['relationships']['stock_code']['required'] = TRUE;
  /* Field: Mã chứng khoán: Label */
  $handler->display->display_options['fields']['stock_code']['id'] = 'stock_code';
  $handler->display->display_options['fields']['stock_code']['table'] = 'stock_code';
  $handler->display->display_options['fields']['stock_code']['field'] = 'stock_code';
  $handler->display->display_options['fields']['stock_code']['relationship'] = 'stock_code';
  $handler->display->display_options['fields']['stock_code']['label'] = 'Mã CK';
  /* Field: Mã chứng khoán: Company_name */
  $handler->display->display_options['fields']['company_name']['id'] = 'company_name';
  $handler->display->display_options['fields']['company_name']['table'] = 'stock_code';
  $handler->display->display_options['fields']['company_name']['field'] = 'company_name';
  $handler->display->display_options['fields']['company_name']['relationship'] = 'stock_code';
  $handler->display->display_options['fields']['company_name']['label'] = 'Công ty';
  /* Field: Giao dịch: Volumes */
  $handler->display->display_options['fields']['volumes']['id'] = 'volumes';
  $handler->display->display_options['fields']['volumes']['table'] = 'user_trading';
  $handler->display->display_options['fields']['volumes']['field'] = 'volumes';
  $handler->display->display_options['fields']['volumes']['label'] = 'Khối lượng';
  $handler->display->display_options['fields']['volumes']['separator'] = '.';
  /* Field: Giao dịch: Price */
  $handler->display->display_options['fields']['price']['id'] = 'price';
  $handler->display->display_options['fields']['price']['table'] = 'user_trading';
  $handler->display->display_options['fields']['price']['field'] = 'price';
  $handler->display->display_options['fields']['price']['label'] = 'Giá (x1000)';
  /* Field: Giao dịch: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'user_trading';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['label'] = 'Tình trạng';
  /* Field: Giao dịch: Command type */
  $handler->display->display_options['fields']['command_type']['id'] = 'command_type';
  $handler->display->display_options['fields']['command_type']['table'] = 'user_trading';
  $handler->display->display_options['fields']['command_type']['field'] = 'command_type';
  $handler->display->display_options['fields']['command_type']['label'] = 'Loại lệnh';
  /* Field: Giao dịch: Trading_type */
  $handler->display->display_options['fields']['trading_type']['id'] = 'trading_type';
  $handler->display->display_options['fields']['trading_type']['table'] = 'user_trading';
  $handler->display->display_options['fields']['trading_type']['field'] = 'trading_type';
  $handler->display->display_options['fields']['trading_type']['label'] = 'Loại giao dịch';
  /* Field: Giao dịch: User trading delete path */
  $handler->display->display_options['fields']['delete_path']['id'] = 'delete_path';
  $handler->display->display_options['fields']['delete_path']['table'] = 'user_trading';
  $handler->display->display_options['fields']['delete_path']['field'] = 'delete_path';
  $handler->display->display_options['fields']['delete_path']['label'] = 'Hành động';
  $handler->display->display_options['fields']['delete_path']['text'] = 'Hủy lệnh';
  /* Sort criterion: Giao dịch: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'user_trading';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Mã chứng khoán: Label */
  $handler->display->display_options['filters']['stock_code']['id'] = 'stock_code';
  $handler->display->display_options['filters']['stock_code']['table'] = 'stock_code';
  $handler->display->display_options['filters']['stock_code']['field'] = 'stock_code';
  $handler->display->display_options['filters']['stock_code']['relationship'] = 'stock_code';
  $handler->display->display_options['filters']['stock_code']['group'] = 1;
  $handler->display->display_options['filters']['stock_code']['exposed'] = TRUE;
  $handler->display->display_options['filters']['stock_code']['expose']['operator_id'] = 'stock_code_op';
  $handler->display->display_options['filters']['stock_code']['expose']['label'] = 'Mã chứng khoán';
  $handler->display->display_options['filters']['stock_code']['expose']['operator'] = 'stock_code_op';
  $handler->display->display_options['filters']['stock_code']['expose']['identifier'] = 'stock_code';
  $handler->display->display_options['filters']['stock_code']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  /* Filter criterion: Giao dịch: Command type */
  $handler->display->display_options['filters']['command_type']['id'] = 'command_type';
  $handler->display->display_options['filters']['command_type']['table'] = 'user_trading';
  $handler->display->display_options['filters']['command_type']['field'] = 'command_type';
  $handler->display->display_options['filters']['command_type']['group'] = 1;
  $handler->display->display_options['filters']['command_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['command_type']['expose']['operator_id'] = 'command_type_op';
  $handler->display->display_options['filters']['command_type']['expose']['label'] = 'Loại lệnh';
  $handler->display->display_options['filters']['command_type']['expose']['operator'] = 'command_type_op';
  $handler->display->display_options['filters']['command_type']['expose']['identifier'] = 'command_type';
  $handler->display->display_options['filters']['command_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  /* Filter criterion: Giao dịch: Trading_type */
  $handler->display->display_options['filters']['trading_type']['id'] = 'trading_type';
  $handler->display->display_options['filters']['trading_type']['table'] = 'user_trading';
  $handler->display->display_options['filters']['trading_type']['field'] = 'trading_type';
  $handler->display->display_options['filters']['trading_type']['group'] = 1;
  $handler->display->display_options['filters']['trading_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['trading_type']['expose']['operator_id'] = 'trading_type_op';
  $handler->display->display_options['filters']['trading_type']['expose']['label'] = 'Loại giao dịch';
  $handler->display->display_options['filters']['trading_type']['expose']['operator'] = 'trading_type_op';
  $handler->display->display_options['filters']['trading_type']['expose']['identifier'] = 'trading_type';
  $handler->display->display_options['filters']['trading_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  /* Filter criterion: Giao dịch: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'user_trading';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    0 => '0',
  );
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Tình trạng';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  $export['user_trading_investor_trading_list_filter'] = $view;

  return $export;
}
