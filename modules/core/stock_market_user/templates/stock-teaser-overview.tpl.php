<div class="stock-code-teaser-overview">
  <h2 class="company-name"><?php print $company_name; ?></h2>
  <div class="prices">
    <div class="price-group-1">
      <ul>
        <li>Giá hiện tại: <span><?php print $current_price; ?></span></li>
        <li>Khối lượng: <span><?php print $current_volumes; ?></span></li>
        <li>Giá mở cửa: <span><?php print $open_price; ?></span></li>
        <li>Giá đóng cửa cửa: <span><?php print $close_price; ?></span></li>
      </ul>
    </div>
    <div class="price-group-2">
      <ul>
        <li>Giá tham chiếu: <span><?php print $reference_price; ?></span></li>
        <li>Giá sàn: <span><?php print $floor_price; ?></span></li>
        <li>Giá trần: <span><?php print $ceil_price; ?></span></li>
        <li>Giá cao nhất: <span><?php print $highest_price; ?></span></li>
        <li>Giá thấp nhất: <span><?php print $lowest_price; ?></span></li>
      </ul>
    </div>
    <div class="price-group-3">
      <?php print $trading_history; ?>
    </div>
  </div>
</div>